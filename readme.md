# Mafia Online 

[![pipeline status](https://gitlab.com/JohnoTheCoder/playmafiaonline/badges/master/pipeline.svg)](https://gitlab.com/JohnoTheCoder/playmafiaonline/commits/master) [![coverage report](https://gitlab.com/JohnoTheCoder/playmafiaonline/badges/master/coverage.svg)](https://gitlab.com/JohnoTheCoder/playmafiaonline/commits/master)


This repository holds the code for the rebuild of the game you can see at playmafia.online which will eventually be released in replacement of the existing game

## Contributing

If you would like to contribute to this repository please contact JohnoTheCoder (aka JohnoTheKid)

## Links

* [Play Mafia Online](https://playmafia.online)
* [Issue Tracker](https://github.com/johnothecoder/playmafiaonline/issues)
* [Documentation](https://github.com/johnothecoder/playmafiaonline/wiki)
* [Development Blog](https://playmafiaonline.wordpress.com)
* [Discord](https://playmafia.online/chat)
* [Twitter](https://twitter.com/playmafiaonline)
* [Facebook](https://facebook.com/playmafiaonline)

## With Thanks

With thanks to the various contributors of open source projects on which this repository relies, and with thanks to the ever supportive community of Mafia Online, especially our community moderators and Beta testers